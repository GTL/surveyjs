var util = require('util');
var fs = require('fs');
var express = require('express');


//
//  Cloud Node
//

//var app = express();
//var server = app.listen(9245); 
//var PORT = process.env['app_port'] || 9245;

//
//      Nodester
//
var app = express();
var server = app.listen(21123); 
var PORT = process.env['app_port'] || 21123;

//
//  Local
//
//var app = express();
//var server = app.listen(8080);

var io = require('socket.io').listen(server, { log: false });


app.configure(function(){
	app.use(express.static(__dirname+'/public'));	
})


app.get('/', function(req,res){
	fs.createReadStream(__dirname + '/index.html').pipe(res);
});

app.get('/results.html', function(req,res){
	fs.createReadStream(__dirname + '/results.html').pipe(res);
});
app.get('/results', function(req,res){
	fs.createReadStream(__dirname + '/results.html').pipe(res);
});
app.get('/version', function(req,res){
	res.writeHeader(200, {'Content-type':'application/json'});
	res.end('{"version":"'+ process.version +'"}');
})

app.get('/stats', function(req,res){
	res.writeHeader(200, {'Content-type':'application/json'});
    //Get Current Stats
    Stats.Current.Clients = Clients.size;
    Stats.Current.Surveys = Surveys.size;
    
    Stats.Memory.Previous = Stats.Memory.Current;
    Stats.Memory.Current = process.memoryUsage();
    
	res.end(JSON.stringify(Stats));
})
app.get('/clear', function(req,res){
	res.writeHeader(200, {'Content-type':'application/json'});
    
    //Clear Current Stats
    Stats.Current.Clients = 0;
    Stats.Current.Surveys = 0;
    Stats.Memory.Current = 0;
    Stats.Memory.Previous = 0;
    Stats.Total.Administrators = 0;
    Stats.Total.Responders = 0;
    Stats.Total.Responses = 0;
    Stats.Total.Surveys = 0;
    
    
	res.end(JSON.stringify(Stats));
})

var CMap = function(linkEntries){
    this.current = undefined;
    this.size = 0;
    this.isLinked = true;

    if(linkEntries === false)
    {
            this.disableLinking();
    }
            
    this.from = function(obj, foreignKeys, linkEntries)
    {
        var map = new Map(linkEntries);

        for(var prop in obj) {
                if(foreignKeys || obj.hasOwnProperty(prop))
                        map.put(prop, obj[prop]);
        }

        return map;
    }
    
    this.noop = function()
    {
            return this;
    }
    
    this.illegal = function()
    {
            throw new Error('can\'t do this with unlinked maps');
    }
    
    this.reverseIndexTableFrom = function(array, linkEntries)
    {
        var map = new Map(linkEntries);

        for(var i = 0, len = array.length; i < len; ++i) {
                var	entry = array[i],
                        list = map.get(entry);

                if(list) list.push(i);
                else map.put(entry, [i]);
        }

        return map;
    }

    this.cross = function(map1, map2, func, thisArg)
    {
        var linkedMap, otherMap;
    
        if(map1.isLinked) {
                linkedMap = map1;
                otherMap = map2;
        }
        else if(map2.isLinked) {
                linkedMap = map2;
                otherMap = map1;
        }
        else Map.illegal();
    
        for(var i = linkedMap.size; i--; linkedMap.next()) {
                var key = linkedMap.key();
                if(otherMap.contains(key))
                        func.call(thisArg, key, map1.get(key), map2.get(key));
        }
    
        return thisArg;
    }

    this.uniqueArray = function(array)
    {
            var map = new Map;
    
            for(var i = 0, len = array.length; i < len; ++i)
                    map.put(array[i]);
    
            return map.listKeys();
    }                                    
};

CMap.prototype.disableLinking = function(){
    this.isLinked = false;
    this.link = Map.noop;
    this.unlink = Map.noop;
    this.disableLinking = Map.noop;
    this.next = Map.illegal;
    this.key = Map.illegal;
    this.value = Map.illegal;
    this.removeAll = Map.illegal;
    this.each = Map.illegal;
    this.flip = Map.illegal;
    this.drop = Map.illegal;
    this.listKeys = Map.illegal;
    this.listValues = Map.illegal;

    return this;
};

CMap.prototype.hash = function(value){
    return value instanceof Object ? (value.__hash ||
            (value.__hash = 'object ' + ++arguments.callee.current)) :
            (typeof value) + ' ' + String(value);
};

CMap.prototype.hash.current = 0;            
CMap.prototype.link = function(entry){
        if(this.size === 0) {
                entry.prev = entry;
                entry.next = entry;
                this.current = entry;
        }
        else {
                entry.prev = this.current.prev;
                entry.prev.next = entry;
                entry.next = this.current;
                this.current.prev = entry;
        }
};

CMap.prototype.unlink = function(entry) {
        if(this.size === 0)
                this.current = undefined;
        else {
                entry.prev.next = entry.next;
                entry.next.prev = entry.prev;
                if(entry === this.current)
                        this.current = entry.next;
        }
};

CMap.prototype.get = function(key) {
        var entry = this[this.hash(key)];
        return typeof entry === 'undefined' ? undefined : entry.value;
};

CMap.prototype.put = function(key, value) {
        var hash = this.hash(key);

        if(this.hasOwnProperty(hash))
                this[hash].value = value;
        else {
                var entry = { key : key, value : value };
                this[hash] = entry;

                this.link(entry);
                ++this.size;
        }

        return this;
};

CMap.prototype.remove = function(key) {
        var hash = this.hash(key);

        if(this.hasOwnProperty(hash)) {
                --this.size;
                this.unlink(this[hash]);

                delete this[hash];
        }

        return this;
};

CMap.prototype.removeAll = function() {
        while(this.size)
                this.remove(this.key());

        return this;
};

CMap.prototype.contains = function(key) {
        return this.hasOwnProperty(this.hash(key));
};

CMap.prototype.isUndefined = function(key) {
        var hash = this.hash(key);
        return this.hasOwnProperty(hash) ?
                typeof this[hash] === 'undefined' : false;
};

CMap.prototype.next = function() {
        this.current = this.current.next;
};

CMap.prototype.key = function() {
        return this.current.key;
};

CMap.prototype.value = function() {
        return this.current.value;
};

CMap.prototype.each = function(func, thisArg) {
        if(typeof thisArg === 'undefined')
                thisArg = this;

        for(var i = this.size; i--; this.next()) {
                var n = func.call(thisArg, this.key(), this.value(), i > 0);
                if(typeof n === 'number')
                        i += n; // allows to add/remove entries in func
        }

        return this;
};

CMap.prototype.flip = function(linkEntries) {
        var map = new Map(linkEntries);

        for(var i = this.size; i--; this.next()) {
                var	value = this.value(),
                        list = map.get(value);

                if(list) list.push(this.key());
                else map.put(value, [this.key()]);
        }

        return map;
};

CMap.prototype.drop = function(func, thisArg) {
        if(typeof thisArg === 'undefined')
                thisArg = this;

        for(var i = this.size; i--; ) {
                if(func.call(thisArg, this.key(), this.value())) {
                        this.remove(this.key());
                        --i;
                }
                else this.next();
        }

        return this;
};

CMap.prototype.listValues = function() {
        var list = [];

        for(var i = this.size; i--; this.next())
                list.push(this.value());

        return list;
}

CMap.prototype.listKeys = function() {
        var list = [];

        for(var i = this.size; i--; this.next())
                list.push(this.key());

        return list;
}

CMap.prototype.toString = function() {
        var string = '[object Map';

        function addEntry(key, value, hasNext) {
                string += '    { ' + this.hash(key) + ' : ' + value + ' }' +
                        (hasNext ? ',' : '') + '\n';
        }

        if(this.isLinked && this.size) {
                string += '\n';
                this.each(addEntry);
        }

        string += ']';
        return string;
};
                                
                                
var CMessage = function(type, data){
    this.type = type;
    this.data = data;
}
var CClient = function(id, socket){
    //Unique ID
    this.id = id;
    //websocket
    this.socket = socket;
    //List of surveys client is registered too
    this.surveys = [];
}

var CSurvey = function(id){
        this.id = id;
        //Survey Status: open, closed
        this.status = "closed";
        //People taking survey
        this.Responders = new CMap();
        //People viewing survey results
        this.Admins = new CMap();

    }
    
var Surveys = new CMap();
var Clients = new CMap();
var nextID = 0;
var Stats ={
    Total: {
        Surveys: 0,
        Responses: 0,
        Responders: 0,
        Administrators: 0
    },
    Current: {
        Surveys: 0,
        Clients: 0
    },
    Memory:{
        Current: 0,
        Previous: 0
    }
}

io.sockets.on('connection', function(socket){
    //Create Client Object and Register to Map
    var client = new CClient(socket.id, socket);
    Clients.put(client.id, client);
    
    //Increment User Count
    nextID++;

    console.log("Got a Client Connection: " + socket.id);
    
    socket.on('message', function(msg){
    
        try{
            msg = JSON.parse(msg);
            console.log("Recived Message Type: " + msg.type);
        }
        catch(e){
            console.log("WebSocket Message Parse Error");
            console.log(e.toString());
        }
    
    });
    
    //Open Survey
    socket.on("OpenSurvey", function(surveyID){
        Stats.Total.Administrators++;
        
        //Get Client Object
        var client = Clients.get(socket.id);
        
        console.log("Opening Survey: " + surveyID);
        var survey = Surveys.get(surveyID);
        
        //If survey does not exist, create it
        if(!survey){
            console.log("Creating Survey for Admin");
            Stats.Total.Surveys++;
            survey = new CSurvey(surveyID);
            Surveys.put(survey.id, survey);
        }
        
        //Add socket to Admins of Survey
        survey.Admins.put(socket.id, client);
        
        //Add surve to client
        client.surveys.push(survey);
        
        //Set Survey status to "open"
        survey.status = "open";
        
        //Open Survey On Registerd Clients
        OpenSurvey(survey);
    });
    
    //Close Survey
    socket.on("CloseSurvey", function(surveyID){
        console.log("Closing Survey: " + surveyID);
        var survey = Surveys.get(surveyID);
        
        if(survey){
           //Remove socket from Admins of Survey
            survey.Admins.remove(socket.id);
            
            //Set Survey status to "close"
            survey.status = "closed";
            
            //Close Survey on All Registered Clients
            CloseSurvey(survey);
        }
        
        
    });
    //Answer from Survey
    socket.on("SurveyAnswer", function(data){
        Stats.Total.Responses++;
        var survey = Surveys.get(data.id);
        
        if(survey && survey.status == "open"){
            console.log("Survey: " + survey.id + ", Answer: " + data.answer);
            SendAnswer(survey, data.answer);
        }
    });
    
    //Register Client With Surveys
    socket.on('Register', function(data){
        Stats.Total.Responders++;
        
        //Get Client Object
        var client = Clients.get(socket.id);
        console.log("Registering Client ID: " + client.id);
        
        for(var i = data.length - 1; i >= 0; i--){
            console.log("Client Registering with Survey: " + data[i].id);
            var survey = Surveys.get(data[i].id);
            
            //If survey does not exist, create it
            if(!survey){
                console.log("Creating Survey for Responder");
                Stats.Total.Surveys++;
                survey = new CSurvey(data[i].id);
                Surveys.put(survey.id, survey);
            }
            
            //Save Survey To Client
            client.surveys.push(survey);
            
            //Register Client with Survey
            survey.Responders.put(client.id, client);
            
            //Send OpenSurve if survey is open
            if(survey.status == "open") {socket.emit("OpenSurvey", survey.id);}
        }
    });
    
    socket.on('disconnect', function(){
        console.log("Socket Disconnected: " + socket.id);
        var client = Clients.get(socket.id);
        
        //Remove Client From All Registerd Surveys
        for(var i = client.surveys.length - 1; i >= 0; i-- ){
            var survey = Surveys.get(client.surveys[i].id);
            
            //Verify survey object has not already been deleted
            if(survey){ 
                
                survey.Responders.remove(client.id);
                survey.Admins.remove(client.id);
                
                if(survey.Admins.size == 0) CloseSurvey(survey);
                
                console.log("Client Removed From: " + survey.id);
                
                if(survey.Responders.size == 0 && survey.Admins.size == 0){
                    Surveys.remove(survey.id);
                    console.log("Survey " + survey.id + "Deleted");
                    survey = null;
                }
            }
            
        }
        
        
        //Remove Client From Client Map
        Clients.remove(client.id);
        client = null;
        console.log("Client Disconnected");
    });
});

var SendAnswer = function(survey, answer){
    console.log("Sending Answer to: " + survey.Admins.size);
    for(var i = survey.Admins.size; i--; survey.Admins.next()){
        survey.Admins.value().socket.emit("SurveyAnswer", {id: survey.id, answer: answer});
    }
}
var OpenSurvey = function(survey){
    for(var i = survey.Responders.size; i--; survey.Responders.next()){
        survey.Responders.value().socket.emit("OpenSurvey", survey.id);
    }
}

var CloseSurvey = function(survey){
    for(var i = survey.Responders.size; i--; survey.Responders.next()){
        survey.Responders.value().socket.emit("CloseSurvey", survey.id);
    }
}
console.log("SurveyJS is up and running");